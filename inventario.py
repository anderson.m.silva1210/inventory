import paramiko
from paramiko import SSHClient
import ipaddress

usuario = input('User: ')
senha = input('Password: ')
hostname = []
ios = []
ip_add = []
serial = []

net4 = ipaddress.ip_address('192.168.0.0')

def sh_ver(ipaddress):
    ssh_connection = paramiko.SSHClient()
    ssh_connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_connection.connect(hostname=str(ipaddress) , username=usuario , password=senha)
    stdin,stdout,stderr = ssh_connection.exec_command("show version")
    output_ok = stdout.read()
    return (output_ok.decode('utf-8'))

def sh_ip(ipaddress):
    ssh_connection = paramiko.SSHClient()
    ssh_connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_connection.connect(hostname=str(ipaddress) , username=usuario , password=senha)
    stdin,stdout,stderr = ssh_connection.exec_command("show ip interface brief")
    output_ok = stdout.read()
    return (output_ok.decode('utf-8'))



for i in range (0,10):
    try:
        version = sh_ver(net4+i)
        version = version.split("\n")
        for line in version:
            if "uptime" in line:
                fields = line.split()
                hostname.append(fields[0])
            if "image file is" in line:
                fields = line.split()
                ios.append(fields[4])
            if "Processor board ID" in line:
                raw = line.split()
                serial.append(raw[3])


    except:
        print("Erro")


for i in range (0,10):
    try:
        brief = sh_ip(net4+i)
        brief = brief.split("\n")

        for line in brief:

            if "Vlan1" in line:
                raw = line.split()
                ip_add.append(raw[1])


    except:
        print("Erro")



output_excel = open("inventario.csv","w")
output_excel.write('hostname;ip-address;ios-version;serial_number;\n')
for i in range(0, len(hostname)):
      output_excel.write(str(hostname[i]) + ';' + str(ip_add[i]) + ';' + str(ios[i]) + ';' + str(serial[i]) + 